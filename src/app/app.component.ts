import {Component} from '@angular/core';

import 'clay-button';
import 'clay-loading-indicator';
import 'clay-checkbox';
import 'clay-portal';

// for some reason, it is not working as expected..
import 'clay-navigation-bar';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  count = 0;

  add() {
    this.count += 1;
  }

  subtract() {
    this.count -= 1;
  }


}
